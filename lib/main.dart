import 'package:flutter/cupertino.dart';
import 'package:todo_app/plan_provider.dart';
import 'package:todo_app/ui/app.dart';

void main(){
  var planProvider = PlanProvider(child: MyToDoApp());
  runApp(planProvider);

}

