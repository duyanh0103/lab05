import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todo_app/ui/plan_creator_screen.dart';

class MyToDoApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "MyTodoApp",
      home: Scaffold(
        body: PlanCreatorScreen(),
      ),
    );
  }
}